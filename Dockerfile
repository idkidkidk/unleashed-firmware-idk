FROM ubuntu:22.04

WORKDIR /app
COPY . .
RUN apt update && apt install -y git 
RUN apt clean

CMD [ "./build.sh"]
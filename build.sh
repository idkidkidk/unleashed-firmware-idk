#!/bin/bash -x

source .env
export DIST_SUFFIX=${VERSION}-idk
export FORCE_NO_DIRTY=yes

rm -rf unleashed-firmware || true
rm -rf base_pack_build || true
rm -rf extra_pack_build || true
rm -fr all-the-apps-base.tgz || true
rm -fr all-the-apps-extra.tgz || true

# Pull the unleashed-firmware image on the $VERSION tag
git clone https://github.com/DarkFlippers/unleashed-firmware.git -b $VERSION
wget https://github.com/xMasterX/all-the-plugins/releases/latest/download/all-the-apps-base.tgz
tar zxvf all-the-apps-base.tgz
wget https://github.com/xMasterX/all-the-plugins/releases/latest/download/all-the-apps-extra.tgz
tar zxvf all-the-apps-extra.tgz

cd unleashed-firmware
git submodule sync
git -c protocol.version=2 submodule update --init --force --recursive
git submodule foreach git config --local gc.auto 0

rm -f build/f7-firmware-C/toolbox/version.*

cp -R ../dolphin/* assets/dolphin/external/
patch -d assets/dolphin/external/ -p0 -s < ../dolphin.patch

./fbt COMPACT=1 DEBUG=0 updater_package
cp -R ../extra_pack_build/artifacts-extra/* assets/resources/apps/
cp -R ../base_pack_build/artifacts-base/* assets/resources/apps/
cp -R ../base_pack_build/apps_data/* assets/resources/apps_data/
./fbt COMPACT=1 DEBUG=0 updater_package
mkdir artifacts-extra-apps
mv dist/f7-C/* artifacts-extra-apps/
mv artifacts-extra-apps/flipper-z-f7-update-${DIST_SUFFIX}.tgz ../
